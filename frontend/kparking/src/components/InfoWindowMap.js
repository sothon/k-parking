import React, {Component} from 'react';
import { Marker, InfoWindow } from "react-google-maps";
import { Button } from 'antd';
import DetailPraking from './DetailPraking'
import './infoWindowMap.css';

class InfoWindowMap extends Component {
    constructor(props){
        super(props);
    
        this.state = {
            isOpen: false
        }
    
    }
    
    handleToggleOpen = () => {    
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    
    handleToggleClose = () => {
        this.setState({
            isOpen: true
        });
    }

    handleDetailPraking = (marker) => (
        this.state.isOpen ? <DetailPraking values={marker} onClose={this.handleToggleOpen} visible={this.state.isOpen}/> :""
    )


    render() {
    console.group("== InfoWindow ===")
    console.log("marker:",this.props.marker)
    console.groupEnd()
        const infoStyle = {
            height: '270px',
            // width: '100px',
            };

        const infoImageStyle = {
            // margin-bottom: '15px',
            width: '150px',
            height: '150px',
            margin: '15px',
            // height: 200px;
            };
        return (
            
                <Marker
                    key={this.props.index}
                    position={{ lat: this.props.position.lat, lng: this.props.position.lng }}
                    icon={this.props.icon} 
                    // label={this.props.index.toString()}
                    onClick={() => this.handleToggleOpen()}
                >
                {this.handleDetailPraking(this.props.marker)}

                {
                this.state.isOpen &&
                <InfoWindow 
                    key={this.props.index + '_info_window'}
                    onCloseClick={() => this.setState({isOpen: false})} >
                    {/* <h1>{this.props.location.venue.name}</h1> */}
                    {/* <div classname='kparkingInfoWindow'> */}
                    <div style={infoStyle}>
                        <img style={infoImageStyle} src = { this.props.marker.thumbnail } alt='imageKparking'/>                            
                        <h3 style={{ textAlign: 'center' }}>{ this.props.marker.title }</h3>
                        {/* <h3 style={{ textAlign: 'center' }}>{`จำนวนที่จอดรถ :  ${this.props.marker.title }` }</h3> */}
                        {/* <h3 style={{ textAlign: 'center' }}>{`ที่จอดรถคงเหลือ :  ${(this.props.marker.number_max - this.props.marker.number_park ) !== 0 ? (this.props.marker.number_max - this.props.marker.number_park ):'เต็มแล้ว'}` }</h3>
                        <h3 style={{ textAlign: 'center' }}>{`อัตราค่าจอดรถ :  ${this.props.marker.price[0]||'รออับเดท'}` }</h3>
                        <h3 style={{ textAlign: 'center' }}>{`เวลาทำการ :  ${this.props.marker.open||'00:00 น.'}` }</h3> */}
                        {/* <div>
                            <Button href={'/parking/'+this.props.index} style={{backgroundColor:'#55B36E', borderColor: '#55B36E', fontSize:'20px', width:'100%'}} type="primary">Park!!</Button>                     
                        </div> */}
                        <div>                            
                            {/* <a href="https://www.google.com/maps/search/?api=1&query=13.7649276,100.5382926" target="_blank">Click here for map</a> */}
                            {/* <a href={`comgooglemaps://?saddr=13.7649276,100.5382926&daddr=${this.props.position.lat},${this.props.position.lng}&directionsmode=driving&zoom=14&views=traffic`} target="_blank">Click here for direction</a>                                              */}
                        </div>
                    </div>                     
                </InfoWindow>
                }    
                </Marker>
        
            )
        }
    }
    
    export default InfoWindowMap;