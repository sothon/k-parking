import React, { Component } from 'react'
import { Input, Button ,Select, Row, Col} from 'antd'
import BackgroundForm from './Background'
import { Link } from 'react-router-dom'

const Option = Select.Option;

export default class AddCar extends Component {
    handleChange(value) {
        console.log(`selected ${value}`);
      }
      
    handleBlur() {
        console.log('blur');
      }
      
    handleFocus() {
        console.log('focus');
      }
  render() {
    return (
      <BackgroundForm>
        <div className='bg-box'>
        <h1 style={{textAlign: 'center',fontWeight: 300,color: '#FFF',marginBottom:20, backgroundColor:'#55B36E'}}>Add Car</h1>
        <div style={{padding:20}}> 
        <Input size="large" placeholder="Car Plate Number" style={{width:'100%',marginBottom:10 }} onChange={this.handleChange} /><br/>
        <Select
            showSearch
            size='large'
            style={{ width: '100%',marginBottom:10 }}
            placeholder="Select a Brand"
            optionFilterProp="children"
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        >
            <Option value="jack">Toyota</Option>
            <Option value="lucy">Honda</Option>
            <Option value="tom">Misubishi</Option>
            <Option value="tom">Nissan</Option>
            <Option value="tom">Mazda</Option>
        </Select>
        
        <Select
            showSearch
            size='large'
            style={{ width: '100%',marginBottom:10  }}
            // style={{ width: 200 }}
            placeholder="Select a color"
            optionFilterProp="children"
            onChange={this.handleChange}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        >
            <Option value="jack">While</Option>
            <Option value="lucy">Red</Option>
            <Option value="tom">Black</Option>
        </Select>
        </div>
        <div style={{display:"flex",flexDirection:'row',width:'100%'}}>
            <div style={{width:'50%'}}>
               <Link to='/signin'>
                <Button size='large' style={{backgroundColor:'#555555', borderColor: '#555555', fontSize:'20px', width:'100%',color:'#fff'}}>Back</Button>
               </Link>
            </div>
            <div style={{width:'50%'}}>     
               <Link to='/payment'>
               <Button size='large' style={{backgroundColor:'#55B36E', borderColor: '#55B36E', fontSize:'20px', width:'100%',color:'#fff'}}>Next</Button>
               </Link>
            </div>
        </div>
        </div>

      </BackgroundForm>
    )
  }
}
