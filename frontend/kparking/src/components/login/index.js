import React, { Component } from 'react'
import { Input, Button } from 'antd'
import BackgroundForm from './Background'
import { FacebookLogin } from 'react-facebook-login-component'
import { GoogleLogin } from 'react-google-login'
import axios from "axios"
import { Link } from 'react-router-dom'

export default class loginForm extends Component {

state = {
 email :""
}

  handleChange = (e)=> {
    console.log(e.target.value)
  }
  handleClick = () => {

  }


  responseFacebook = async (response) => {
    const tokenfromdata = await axios({
        url: "http://localhost:3000/parking/insertcustomerbyfacebook",
        method: "post",
        headers: {
            authorization: JSON.stringify(response)
        }
    })
    this.props.setLogin()
    window.sessionStorage.setItem('jwtToken', response.accessToken);
}
responseGoogle = async (response) => {

  const tokenfromdata = await axios({
      url: "http://localhost:3000/parking/insertcustomerbygmail",
      data: {
          'email': response.w3.U3,
          'password': "",
          'firstname': response.w3.ig,
          'lastname': "",
          'facbook': "",
          'gmail': response.googleId,
          'typelogin': "gmail"
      },
      method: "post",
      headers: {
          authorization: response.Zi.access_token
      }
  })
  // console.log(a)
  this.props.setLogin()
  this.setredirect()
  window.sessionStorage.setItem('jwtToken', tokenfromdata.data.token);
}
  render() {
    return (
      <BackgroundForm >
      <div style={{height:'100%',padding:20, margin: 0,justifyContent: 'center', display: 'flex',flexDirection: 'column'}}>
        <h1 style={{textAlign: 'center',fontWeight: 300,color: '#FFF'}}>Please sign in.</h1>
        <div style={{flexDirection: 'column',display: 'flex',marginTop: '10px'}}>
        <FacebookLogin
          href="https://app.kparking.co/map"
          socialId="301547820466352"
          language="en_US"
          scope="public_profile,email"
          responseHandler={this.responseFacebook}
          xfbml={true}
          fields="id,email,name"
          version="v2.5"
          className="loginBtn loginBtn--facebook"
          buttonText="Login with Facebook"
          style={{width:'100%'}}
          />
        <GoogleLogin
          clientId="444103854770-7s1f9k5tso4q5m0n6f14r5ag2q3r0oog.apps.googleusercontent.com"
          buttonText="Login with google"
          onSuccess={this.responseGoogle}
          onFailure={this.responseGoogle}
          className="loginBtn loginBtn--gmail"
          style={{width:'100%'}}
          />
        </div>
        <div style={{marginTop:10,marginBottom:10,flexDirection: 'row',display: 'flex',alignItems: 'center'}}>
          <hr width='100%'/>
          <h3 style={{color:'#8e8e8e',marginLeft:10, marginRight:10}}>or</h3>
          <hr width='100%'/>
        </div>
        <div style={{flexDirection: 'column',display: 'flex'}}>
          <label style={{color:'#fff'}}>Email</label>
          <Input size="large" placeholder="e.g.name@company.com" style={{width:'100%'}} onChange={this.handleChange} /><br/>
          <Link to="/addcar">
            <Button size='large' type='primary' onClick={this.handleClick} style={{backgroundColor:'#55B36E', borderColor:'#55B36E',width:'100%',fontSize:'20px'}}>Next</Button>
          </Link>
        </div>
        <div style={{justifyContent: 'center',display: 'flex',marginTop:10,color:'#fff'}} >
          Don't have an account ? <a href="#">Create one</a>
          </div>
      </div>
      </BackgroundForm>
    )
  }
}
