import React, { Component } from 'react'
import { Row, Col } from 'antd'

export default class Background extends Component {
  render() {
    return (
      <div className='bg-image' style={{ height:'100%'}}>
        <Row style={{ height:'100%',display: 'flex' }}>
          <Col xs={0} sm={0} md={0} lg={5} xl={5} ></Col>
          <Col xs={24} sm={24} md={24} lg={14} xl={14} style={{height:'100%',padding:20, margin: 0,justifyContent: 'center', display: 'flex',flexDirection: 'column'}}>
            {this.props.children}
          </Col>
          <Col xs={0} sm={0} md={0} lg={5} xl={5} ></Col>
        </Row>
      </div>
    )
  }
}
