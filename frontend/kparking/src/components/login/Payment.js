
import React, { Component } from 'react'
import { Input, Button ,Radio, Row, Col} from 'antd'
import BackgroundForm from './Background'
import { Link } from 'react-router-dom'
const RadioGroup = Radio.Group;
export default class Payment extends Component {
  handleChange(e) {
      console.log(e.target.value);
  }
  render() {
    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
      color:'#fff',
      fontSize:20,
    }
    return (
      <BackgroundForm>
        <div className='bg-box'>
        <h1 style={{textAlign: 'center',fontWeight: 300,color: '#FFF',marginBottom:20, backgroundColor:'#55B36E'}}>Payment Method</h1>
        <div style={{display:'flex', justifyContent:'center'}}>
          <div style={{marginTop:30, marginBottom:30}}>
            <RadioGroup name="Payment" onChange={this.handleChange} size='large'>
              <Radio style={radioStyle} value='AirPay'>Air Pay</Radio>
              <Radio style={radioStyle} value='PayAtParking'>Pay at Parking</Radio>
            </RadioGroup>
          </div>
        </div>
       
        <br/>
        <div style={{display:"flex",flexDirection:'row',width:'100%'}}>
            <div style={{width:'50%'}}>
               <Link to='/'>
                <Button size='large' style={{backgroundColor:'#555555', borderColor: '#555555', fontSize:'20px', width:'100%',color:'#fff'}}>Back</Button>
               </Link>
            </div>
            <div style={{width:'50%'}}>     
               <Link to='/thankyou'>
               <Button size='large' style={{backgroundColor:'#55B36E', borderColor: '#55B36E', fontSize:'20px', width:'100%',color:'#fff'}}>Next</Button>
               </Link>
            </div>
        </div>
        </div>

      </BackgroundForm>
    )
  }
}
