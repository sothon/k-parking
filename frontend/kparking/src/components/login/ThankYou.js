import React, { Component } from 'react'
import {  Button } from 'antd'
import { Link } from 'react-router-dom'


export default class ThankYou extends Component {
    handleClick=()=>{
        console.log('test long')
        this.props.setLogin()
        // <Redirect to='/map' />
    }
  render() {
    return (
      <div className='bg-thankyou'> 
          <img src='./../images/icons/premiumIcon@3x.png' alt="premiumIcon"/>
          <h1 className='text-thx'>Thank You for using the service</h1>
          <div style={{width:'100%'}}>
            <Link to='/map'>
                <Button  onClick={this.handleClick} size='large' style={{backgroundColor:'#555555', borderColor: '#555555', fontSize:'20px', width:'100%',color:'#fff'}}>Route</Button>
            </Link>
          </div>
      </div>
    )
  }
}

 