// import React, { Component } from 'react';
import React from 'react';
import InfoWindowMap from './InfoWindowMap'
import { Button } from 'antd';

const { compose, withProps, withHandlers, lifecycle } = require("recompose");
const { withScriptjs, withGoogleMap, GoogleMap, Marker } = require("react-google-maps");
const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");

const MapWithAMarkerClusterer = compose(
  lifecycle({
    componentWillReceiveProps(nextProps, nextState) {
      console.log('nextProps=>', nextProps)
      if (nextProps.location.length !== 0) {
        this.setState({
          center: nextProps.center,
          zoom: 15,
          location: nextProps.location,
        }, () => {
        });
      }
    },
    componentWillMount() {
      this.setState({
        //center: {lat: 13.7627583, lng: 100.5370845},        
        center: { lat: 13.8660686, lng: 100.6136229 },
        location: { lat: 13.7627583, lng: 100.5370845 },
        // center: {},
        // location: {},
        zoom: 15,
      })
    },
    componentDidUpdate(prevProps, prevState) {

    },
  }),
  withProps({
    // googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBMjuCtjH4TltN0wNvt8i7qquJSjcNDzP8&v=3.exp&libraries=geometry,drawing,places",
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyC0naKS6kTwg_U7ZPqBm0hjsUQny_uFEPI&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{
      position: 'absolute',
      top: 60,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'flex-end',
      alignItems: 'center',
    }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withHandlers({
    onMarkerClustererClick: () => (markerClusterer) => {
      // const clickedMarkers = markerClusterer.getMarkers()
      // console.log(`Current clicked markers length: ${clickedMarkers.length}`)
      // console.log(clickedMarkers)
    },
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    //defaultZoom={ 2 }
    //defaultCenter={{ lat: 13.7627583, lng: 100.5370845 }}
    zoom={props.zoom}
    center={props.center}

    options={{
      panControl: false,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false,
      zoomControl: false
    }}
  >
    <MarkerClusterer
      onClick={props.onMarkerClustererClick}
      averageCenter
      enableRetinaIcons
      gridSize={60}
    >
      {props.markers.map(marker => (

        <InfoWindowMap
          key={marker._id}
          index={marker._id}
          //position={{ lat: marker.lat, lng: marker.lng + 0.0022 }}  
          position={{ lat: marker.lat, lng: marker.lng }}
          icon={marker.marker2x}
          marker={marker}
        />

      ))}
    </MarkerClusterer>
    <Marker position={props.location} />
  </GoogleMap>
);

class Map extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      markers: [],
      location: {},
      center: {},
    }
  }

  componentWillMount() {
    // this.setState({ 
    //   markers: [] ,
    // })
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps, nextState) {
    this.setState(
      {
        location: nextProps.location,
        center: nextProps.center,
        markers: nextProps.markers,
      })
  }

  onLocationClick = () => {
    this.props.onLocationClick();
  }

  render() {
    return (
      <div>
        <MapWithAMarkerClusterer
          markers={this.state.markers}
          location={this.state.location}
          center={this.state.center}
        />
        <Button type="dashed" shape="circle" size='large'
          onClick={this.onLocationClick}
          style={{
            position: 'fixed',
            bottom: '50px',
            right: '15px',
            width: '64px',
            height: '64px',
            borderColor: 'rgba(0,0,0,0.01)',
            background: 'rgba(0,0,0,0.01)',
            backgroundImage: `url('/images/icons/gps-fixed-indicator.png')`
          }}
        />
      </div>
    )
  }
}

export default Map;


