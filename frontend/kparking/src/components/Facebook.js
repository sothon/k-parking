import React, { Component } from 'react'
import { FacebookLogin } from 'react-facebook-login-component'
// import FacebookLogin from "react-facebook-login";
import { Form, Input, Button, Checkbox, Row, Col, Select, AutoComplete } from 'antd'
import './facebook.css'
import { Redirect } from 'react-router'
import { GoogleLogin } from 'react-google-login'
import axios from "axios"

const FormItem = Form.Item
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option

class Login extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            redirectregister: false
        }
    }
    setredirect = () => {
        this.setState({ redirectregister: true })
    }
    handleSubmit = async (e) => {
        e.preventDefault();
        this.props.form.validateFields(async (err, values) => {
            const tokenfromdata = await axios({
                url: "http://localhost:3000/parking/login",
                method: "post"
                ,
                data: {
                    'email': values.username, 'password': values.password
                }

            })
            console.log(tokenfromdata.data)
            if (Number(tokenfromdata.data.token) !== 0) {
                this.props.setLogin()
                window.sessionStorage.setItem('jwtToken', tokenfromdata.data.token);
            }
            else if (Number(tokenfromdata.data.token) === 0) {
                alert('ไม่มี Email นี้ในระบบ / invalid email ')
            } else {
                alert('Email หรือ PassWord ไม่ถูกต้อง / Invalid Email Or Password ')
            }
        });
    }


    responseFacebook = async (response) => {
        const tokenfromdata = await axios({
            url: "http://localhost:3000/parking/insertcustomerbyfacebook",
            method: "post",
            headers: {
                authorization: JSON.stringify(response)
            }
        })
        this.props.setLogin()
        window.sessionStorage.setItem('jwtToken', response.accessToken);
    }

    responseGoogle = async (response) => {

        const tokenfromdata = await axios({
            url: "http://localhost:3000/parking/insertcustomerbygmail",
            data: {
                'email': response.w3.U3,
                'password': "",
                'firstname': response.w3.ig,
                'lastname': "",
                'facbook': "",
                'gmail': response.googleId,
                'typelogin': "gmail"
            },
            method: "post",
            headers: {
                authorization: response.Zi.access_token
            }
        })
        // console.log(a)
        this.props.setLogin()
        this.setredirect()
        window.sessionStorage.setItem('jwtToken', tokenfromdata.data.token);
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        // const responseFacebook = (response) => {
        //     console.log(response);
        //     this.signup(response, 'facebook');
        // }
        // const responseGoogle = (response) => {
        //     console.log("google console");
        //     console.log(response);

        // }

        return (
            <Row>
                <Col xs={2} sm={4} md={4} lg={4} xl={4}></Col>
                <Col xs={20} sm={16} md={16} lg={16} xl={16}>
                    <div style={{
                        // height: '80%',
                        paddingTop: '30%',
                        // paddingBottom: '',
                        margin: '0',
                        backgroundImage: `url('/images/kparking/bg.png')`,
                        backgroundPosition: 'center',
                        backgroundRepeat: 'no-repeat',
                        backgroundSize: 'cover',
                    }}>
                        {/* // redirect after success register user */}
                        {this.state.redirectregister && <Redirect to='/map' />}
                        <Form style={{ textAlign: 'center' }}>
                            <FormItem style={{ paddingBottom: '10%' }}>
                                <div style={{ color: '#ffffff', display: 'inline' }}>
                                    <span style={{ fontSize: '70px' }}>K</span>
                                    <span style={{ fontSize: '57px' }}>P</span>
                                    <span style={{ fontSize: '46px' }}>ARKING</span>
                                </div>
                                <div style={{ color: '#ffffff' }}>
                                    <p style={{ fontSize: '14px', lineHeight: '4px', marginTop: '-8px' }}>ANYTIME ANYPLACE ANYPRICE</p>
                                </div>
                            </FormItem>
                            <Form onSubmit={this.handleSubmit} className="login-form">
                                <FormItem style={{ 'marginBottom': '0px' }}>

                                    {getFieldDecorator('username', {
                                        rules: [{ required: true, message: 'Please input your username!' }],
                                    })(
                                        <Input style={{ 'width': '70%', 'marginBottom': '1%' }} placeholder="Username" />
                                    )}
                                </FormItem>
                                <FormItem style={{ 'marginBottom': '0px' }}>
                                    {getFieldDecorator('password', {
                                        rules: [{ required: true, message: 'Please input your Password!' }],
                                    })(
                                        <Input style={{ 'width': '70%', 'marginBottom': '1%' }} type="password" placeholder="Password" />
                                    )}
                                </FormItem>
                                <FormItem style={{ 'marginBottom': '0px' }}>
                                    {getFieldDecorator('remember', {
                                        valuePropName: 'checked',
                                        initialValue: true,
                                    })(
                                        <Checkbox>Remember me</Checkbox>
                                    )}
                                    <a href="/parking/Resetpassword">Forgot password</a>
                                </FormItem>
                                <FormItem>
                                    <Button style={{ 'width': '30%' }} type="primary" htmlType="submit" className="login-form-button">
                                        Log in
                                </Button>
                                    <div><a href="/parking/Register">Sing Up</a></div>
                                </FormItem>
                            </Form>
                            <FormItem style={{ 'marginBottom': '10%' }}>
                                <FacebookLogin
                                    href="https://app.kparking.co/map"
                                    socialId="301547820466352"
                                    language="en_US"
                                    scope="public_profile,email"
                                    responseHandler={this.responseFacebook}
                                    xfbml={true}
                                    fields="id,email,name"
                                    version="v2.5"
                                    className="loginBtn loginBtn--facebook"
                                    buttonText="Login with Facebook"
                                />

                                <GoogleLogin
                                    clientId="444103854770-7s1f9k5tso4q5m0n6f14r5ag2q3r0oog.apps.googleusercontent.com"
                                    buttonText="Login with google"
                                    onSuccess={this.responseGoogle}
                                    onFailure={this.responseGoogle}
                                    className="loginBtn loginBtn--gmail"
                                />
                            </FormItem>
                            <FormItem style={{ paddingBottom: '10%' }}>
                                <div style={{ color: '#ffffff' }}>
                                    <p style={{ fontSize: '13px', lineHeight: '4px', marginTop: '10px' }}>Parking lot owner?</p>
                                </div>
                            </FormItem>
                        </Form>
                    </div>
                </Col>
                <Col xs={2} sm={4} md={4} lg={4} xl={4}></Col>
            </Row>

        )
    }
}

const WrappedNormalLoginForm = Form.create()(Login);
export default WrappedNormalLoginForm




