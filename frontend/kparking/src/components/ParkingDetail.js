import React, { Component } from 'react'
// import { Provider } from 'react-redux'
import { Layout, Breadcrumb, Affix, Button, Modal, Row, Col, Radio } from 'antd';
import { Rate } from 'antd';
import MyLayout from './layout'
import './App.css';
// import './ParkingDetail.css';
const QRCode = require('qrcode.react');
const _ = require('lodash');
const { Content, Footer } = Layout;
const ButtonGroup = Button.Group;
const { compose } = require("recompose");
const { withScriptjs, withGoogleMap, GoogleMap, Marker } = require("react-google-maps");

// ReactDOM.render(<Rate disabled defaultValue={2} />, mountNode);
const infoImageStyle = {
  // margin-bottom: '15px',
  // width: '100%',
  margin: '15px',
  height: '200px',
  width: '285px'
};
const RadioGroup = Radio.Group;
class ParkingDetail extends Component {

  constructor(props) {
    super(props);

    this.state = {
      ModalText: 'We will direct you to the parking lot and you can pay in cash upon arrival.',
      visible: false,
      confirmLoading: false,
      parkingRemain: 0,
      value: 1,
      checkin: false

    }
  }

  componentDidMount() {
    const { match: { params } } = this.props;
    // console.log(params.id)
    const url = [`https://app.kparking.co/api/parking/${params.id}`].join("")
    // console.log(url)
    fetch(url)
      .then(res => res.json())
      .then(data => {
        let remain = (_.isInteger(data.parkings[0].number_max) ? data.parkings[0].number_max : 0) - (_.isInteger(data.parkings[0].number_park) ? data.parkings[0].number_park : 0)
        this.setState({
          parking: data.parkings,
          parkingReamin: remain
        });
      });

  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  handleOk = async () => {
    if (this.state.parkingReamin > 0) {
      await this.parkingCheckIn(this.state.parking[0]._id)
    } else {
      await this.parkingReset(this.state.parking[0]._id)
    }

    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });

    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  }
  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  }
  onChange = (e) => {

    this.setState({
      value: e.target.value,
    });
  }

  async parkingCheckIn(id) {
    const url = `https://app.kparking.co/api/parking/${id}/checkin`
    console.log(url)
    fetch(url, {
      method: 'PUT',
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
      .then(res => res.json())
      .then(data => {
        this.setState({
          checkin: data.checkin
        });
        window.location.href = `https://www.google.com/maps/search/?api=1&query=${this.state.parking[0].lat},${this.state.parking[0].lng}`
      });
  }

  async parkingReset(id) {
    const url = `https://app.kparking.co/api/parking/${id}/chreseteckin`
    console.log(url)
    fetch(url, {
      method: 'PUT',
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
      .then(res => res.json())
      .then(data => {
        this.setState({
          checkin: data.checkin
        });
        window.location.href = `https://www.google.com/maps/search/?api=1&query=${this.state.parking[0].lat},${this.state.parking[0].lng}`
      });

  }


  render() {
    const { visible, confirmLoading, ModalText } = this.state;
    const MapWithAMarker = compose(
      withScriptjs,
      withGoogleMap
    )(props =>
      <GoogleMap
        defaultZoom={13}
        defaultCenter={{ lat: 13.9030805, lng: 100.5308431 }}
      >
        {_.isArray(this.state.parking) && <Marker
          position={{ lat: this.state.parking[0].lat, lng: this.state.parking[0].lng }}
        />}

      </GoogleMap>
    );
    // const Parking = ({ store }) => (
    const Parking = () => (
      // <Provider store={store}>
      <Content style={{ padding: '0 20px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item><a href="/map">Home</a></Breadcrumb.Item>
          <Breadcrumb.Item>Parking</Breadcrumb.Item>
        </Breadcrumb>
        <div style={{ background: '#fff', padding: 10, minHeight: 280 }}>
          <div>
            <h2>{_.isArray(this.state.parking) && this.state.parking[0].title}</h2>
            {_.isArray(this.state.parking) && <Rate disabled defaultValue={this.state.parking[0].rate} />}
            {!_.isArray(this.state.parking) && <Rate disabled defaultValue={0} />}
          </div>
          <div>
            {_.isArray(this.state.parking) && <img style={infoImageStyle} src={this.state.parking[0].photo} alt='imageKparking' />}
          </div>
          <div>
            <h3>Description</h3>
            {/* <p>ชื่อสถานที่จอดรถ : {_.isArray(this.state.parking) && this.state.parking[0].title}</p> */}
            <p>ที่อยู่ : {_.isArray(this.state.parking) && this.state.parking[0].address}</p>
            {_.isArray(this.state.parking) &&
              <ul>
                {
                  this.state.parking[0].detail.map((value, index) => (
                    <li key={index}><p>{value}</p></li>
                  ))}
              </ul>
            }
            <p>
              จำนวนที่จอดรถ : {_.isArray(this.state.parking) && this.state.parking[0].number_max
                + ' คงเหลือ ' + this.state.parkingReamin
              }
            </p>
            <p>อัตราค่าจอดรถ : </p>
            {_.isArray(this.state.parking) &&
              <ul>
                {
                  this.state.parking[0].price.map((value, index) => (
                    <li key={index}><p>{value}</p></li>
                  ))}
              </ul>
            }
            <p>ชื่อ : {_.isArray(this.state.parking) && this.state.parking[0].owner}</p>
            <p>เบอร์โทรศัพท์ : {_.isArray(this.state.parking) && this.state.parking[0].phone}</p>
            {/* <p>ID Line : peck1968</p>           */}
          </div>
          <div>
            <h3>Location</h3>
            <MapWithAMarker
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMjuCtjH4TltN0wNvt8i7qquJSjcNDzP8&v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={<div style={{ height: `400px` }} />}
              mapElement={<div style={{ height: `100%` }} />}
            />

          </div>

          {/* <div>
              <h3>Review(3)</h3>
            </div> */}
        </div>
      </Content>
      //  </Provider>

    )

    return (
      <div>
        <MyLayout />
        <Row>
          <Col xs={2} sm={4} md={4} lg={4} xl={4}></Col>
          <Col xs={20} sm={16} md={16} lg={16} xl={16}>
            <Parking />
          </Col>
          <Col xs={2} sm={4} md={4} lg={4} xl={4}></Col>
        </Row>
        <Affix offsetBottom={0}>
          <Footer style={{ textAlign: 'center', padding: 10 }}>
            <ButtonGroup>
              <Button disabled style={{ backgroundColor: 'white', color: 'black', height: '50px', width: '200px' }}>
                <h5 style={{ paddingTop: 5 }}>฿30/hour</h5>
              </Button>
              {(this.state.parkingReamin > 0) && <Button type="primary"
                onClick={this.showModal}
                style={{ backgroundColor: '#55B36E', borderColor: '#55B36E', height: '50px', width: '150px' }}>
                <h5 style={{ paddingTop: 5 }}>Park!</h5>
              </Button>
              }
              {(this.state.parkingReamin === 0) &&
                <Button disabled
                  onClick={this.showModal}
                  style={{ backgroundColor: '#55B36E', borderColor: '#55B36E', height: '50px', width: '150px' }}>
                  <h5 style={{ paddingTop: 5 }}>Full!</h5>
                </Button>
              }
            </ButtonGroup>
            {
              _.isArray(this.state.parking) &&
              // <Modal title="Park at Silom Complex Building?"     
              <Modal title={'Park at ' + this.state.parking[0].title}
                visible={visible}
                onOk={this.handleOk}
                confirmLoading={confirmLoading}
                onCancel={this.handleCancel}
              >
                <p>{ModalText}</p>
                {/* <p>testqr</p> */}
                <QRCode value="https://pay.airpay.in.th/start?order_id=SBIPS_1448261830_311964342&app_id=100001&source=qr&key=nrqL56b9b2e1a9c15b88" size='200' />
                <div>
                  <RadioGroup onChange={this.onChange} value={this.state.value}>
                    <Radio value={1}>Airpay</Radio>
                  </RadioGroup>
                </div>
              </Modal>
            }

          </Footer>
        </Affix>
      </div>
    )
  }
}

export default ParkingDetail


