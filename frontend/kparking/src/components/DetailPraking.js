import React, { Component } from 'react'
import { Button, Row, Col, Affix, Avatar, Drawer,Card} from 'antd';

export default class DetailPraking extends Component {

  render() {
console.group("=== detailPraking ===")
console.log("values",this.props.values)
console.groupEnd("=== detailPraking ===")
const {onClose,visible} = this.props
    return (
        // <Affix offsetBottom={0} >
        // <div style={{backgroundColor:"#fff"}}>
        //     <Row  justify="center">
        //         <Col span={12} >
        //         {
        //             `ที่จอดรถคงเหลือ :  
        //             ${(this.props.values.number_max - this.props.values.number_park ) !== 0 
        //                 ? (this.props.values.number_max - this.props.values.number_park )
        //                 :'เต็มแล้ว'}` 
        //             }
        //         </Col>
        //         <Col span={12} >
        //             <Avatar shape="square" size={64} src={this.props.values.thumbnail}/>
        //         </Col>
        //     </Row>
        //     <Row type="flex" justify="center">
        //         <Col span={12} >{`อัตราค่าจอดรถ :  ${this.props.values.price[0]||'รออับเดท'}` }</Col>
        //         <Col span={12} >{`เวลาทำการ :  ${this.props.values.open||'00:00 น.'}` }</Col>
        //     </Row>
        //     <Row type="flex" justify="center">
        //         <Col span={18} ><Button block style={{backgroundColor:'#55B36E', borderColor: '#55B36E', fontSize:'20px',color:'fff'}}>Prak</Button></Col>
        //         <Col span={6} ><Button block onClick={()=>close()} style={{ fontSize:'20px',color:'fff'}} type="danger">Close</Button></Col>
        //     </Row>
        // </div>
        // </Affix>

        <Drawer
          title={this.props.values.title||""}
          placement='bottom'
          closable={false}
        //   onClose={onClose}
          visible={visible}
          style={{padding:0,display:"flex",flexDirection:'row' }}
        >
         <div style={{flex:1,paddingTop: 10}}>
            <div style={{display:"flex",flexDirection:'row',width:'100%'}}>
                <div style={{width:'50%',textAlign:"center"}}>
                <img  src={this.props.values.thumbnail} width={90} height={90}/>
               </div>
                <div style={{width:'50%',textAlign:"center"}}>
                <span>ที่จอดว่าง :</span>
                <p style={{textAlign:"center",margin:0}}>{(this.props.values.number_max - this.props.values.number_park )}</p>
                <span>คัน</span>
                 </div>
            </div>
            <hr className='style-two'/>
            <div style={{display:"flex",flexDirection:'row',width:'100%',marginBottom:10}}>
                <div style={{width:'50%', textAlign:"center" }}>
                <span>อัตราค่าจอดรถ :</span>
                <h5 style={{textAlign:"center"}}>{this.props.values.price[0]||'รออับเดท'}</h5>
                 </div>
                <div style={{width:'50%',textAlign:"center"}}>
                <span>เวลาทำการ :</span>
                <h5 style={{textAlign:"center"}}>{this.props.values.open||'00:00 น.'}</h5>
               </div>
            </div>
            <div style={{display:"flex",flexDirection:'row',width:'100%'}}>
                <div style={{width:'70%'}}>
                    <Button onClick={()=>alert('การจองเสร็จสมบูรณ์')} style={{backgroundColor:'#55B36E', borderColor: '#55B36E', fontSize:'20px', width:'100%'}} >Park!!</Button>                     
                </div>
                <div style={{width:'30%'}}>
                    <Button onClick={onClose} type='primary' style={{backgroundColor:'#555555', borderColor: '#555555', fontSize:'20px', width:'100%'}} >X</Button>                     
                </div>
            </div>
        </div>
        {/* <Row type="flex" justify="center">
            <Col span={12} >{`อัตราค่าจอดรถ :  ${this.props.values.price[0]||'รออับเดท'}` }</Col>
            <Col span={12} >{`เวลาทำการ :  ${this.props.values.open||'00:00 น.'}` }</Col>
        </Row>
        <Row type="flex" justify="center">
            <Col span={18} ></Col>
            <Col span={6} ></Col>
        </Row> */}
        </Drawer>

    )
  }
}
