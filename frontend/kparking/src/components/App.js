import React, { Component } from 'react'
import Map from './Map'
import MyLayout from './layout'
import Complete from './complete'


// import { Input } from 'antd';
// import db from '../libs/markers.db.json';
// const Search = Input.Search;
import Suggest from './suggest'

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      center: {},
      location: {},
      // center: { lat: 13.8660686, lng: 100.6136229 },
      // location: { lat: 13.7627583, lng: 100.5370845 },
      markers: [],
    }
  }

  componentWillMount() {
    this.getGeolocation()
  }

  componentDidMount() {
    // console.log('test app.js')
    this.getParkinglocation()
  }

  getGeolocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log(position)
        this.setState({
          location: {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          },
          center: {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }
        })
      }, () => {
        // console.log('navigator disabled');
      });
    }
  }

  getParkinglocation = () => {
    const url = [
      // Length issue
      `https://app.kparking.co/api`,
      // `http://127.0.1.1:3000`,
      `/parking/`
    ].join("")
    // console.log(url)
    fetch(url)
      .then(res => res.json())
      .then(data => {
        // console.log('data.parkings')
        // console.log(data.parkings)
        this.setState({
          markers: data.parkings
        });
      });
  }

  handleSearchValue = (value) => {
    console.log("handleSearchValue: ");
    console.log(value);

    this.setState({
      markers: value,
      center: {
        lat: value[0].lat,
        lng: value[0].lng
      }
    })

    console.log("this.state.markers: ");
    console.log(this.state.markers);
  }

  handleLocationClick = () => {
    console.log("handleLocationClick");
    this.getGeolocation()
  }

  render() {

    console.group("==== check ====")
    console.log("location :",this.state.location)
    console.log("markers :",this.state.markers)
    console.log("center :",this.state.center)
    console.groupEnd()
    return (
      <div style={{ marginLeft: '1px' }}>
        <MyLayout logined={this.props.logined} setLogout={this.props.setLogout} />
        {/* <Complete
          onSearchChange={this.handleSearchValue.bind(this)}
        /> */}
        {/* <Suggest /> */}
        <Map
          location={this.state.location}
          markers={this.state.markers}
          center={this.state.center}
          onLocationClick={this.handleLocationClick.bind(this)}
        />
      </div>
    )
  }
}

export default App