// import React, {Component} from 'react'
import React from 'react'
import { Layout, Icon } from 'antd'
import MenuSlide from './MenuSlide'
import './App.css'

// import { Input } from 'antd';

// const Search = Input.Search;
const { Header } = Layout
// const { Header } = Layout;

export default class MyLayout extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      visible: false
    }
  }
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  }
  Logout = () => {
    this.props.setLogout()
    window.sessionStorage.removeItem('jwtToken')
  }
  onClose = () => {
    this.setState({
      visible: false,
    });
  }
  render() {
    return (
      <div>
        <Layout className='layout'>
          <Header className='bg'>
            <div style={{ color: '#ffffff', display: 'inline' }}>
              <span style={{ fontSize: '40px' }}>K</span>
              <span style={{ fontSize: '33px' }}>P</span>
              <span style={{ fontSize: '26px' }}>ARKING</span>
              <Icon type="menu-fold" theme="outlined" className="absoluteIcon" onClick={this.showDrawer} />
              <MenuSlide
                onClose={this.onClose}
                visible={this.state.visible}
                setLogout={this.props.setLogout}
              />
            </div>
            <div style={{ color: '#ffffff' }}>
              <p style={{ fontSize: '8px', lineHeight: '1px', marginTop: '-10px' }}>ANYTIME ANYPLACE ANYPRICE</p>
            </div>
          </Header>
        </Layout>
      </div>
    )
  }
}
