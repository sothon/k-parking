import React from 'react'
import PropTypes from 'prop-types'
// import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import App from './App'
import ParkingDetail from './ParkingDetail'
import WrappedNormalLoginForm from './Facebook'
import WrappedRegistrationForm from './Register'
import WrappedResetpasswordForm from './Resetpassword'
import ThankyouForm from './login/ThankYou'
import LoginForm from './login/'
import AddCar from './login/AddCar'
import Payment from './login/Payment'
import axios from 'axios'
// const Root = ({ store }) => (

// window.sessionStorage.removeItem('jwtToken')
// ได้ยินไม ไม่


class Root extends React.Component {
  state = {
    logined: false
    // ,redirectregister: false
  }
  componentDidMount() {
    this.checkAPI()
  }
  checkAPI = async () => {
    // const data = axios.post()
    var data = window.sessionStorage.getItem('jwtToken')
    const tokenfromdata = await axios({
      url: 'http://localhost:3000/api/posts',
      method: 'post',
      headers: {
        authorization: 'bearer ' + data
      }
    })
    console.log(tokenfromdata)
    this.setState({ logined: tokenfromdata.data })
    // var authOptions = {
    //   method: 'POST',
    //   url: 'http://localhost:3000/api/posts',
    //   data: null,
    //   headers: {
    //     authorization: 'bearer ' + data,
    //     'Content-Type': 'application/x-www-form-urlencoded'
    //   },
    // json: true
    // }
    // axios(authOptions)
    //   .then(function (response) {
    //     return response.data
    //   })
    //   .catch(function (error) {
    //     return error
    //   })
    // return tokenfromdata.data
  }
  setLogin = () => {
    this.setState({ logined: true })
  }
  setLogout = () => {
    this.setState({ logined: false })
  }
  // setredirect = () => {
  //   this.setState({ redirectregister: false })
  // }
  render() {
    const { logined } = this.state
    // const { redirect } = this.state.redirect

    // let date2 = await checkAPI()
    // console.log(date2)
    return (
      // <Provider store={store}>
      // <Provider>
      // https://reacttraining.com/react-router/web/api/Redirect
      <Router >
        <Switch >
          {/* <Route exact path='/' render={() => (logined ? (<Redirect to='/map' />) : (<WrappedNormalLoginForm logined={logined} setLogin={this.setLogin} />))} /> */}
          <Route exact path='/' render={() => (logined ? (<Redirect to='/map' />) : (<LoginForm />))} />
          <Route path='/signin' render={() => (logined ? (<Redirect to='/map' />) : (<LoginForm />))} />
          <Route path='/map' render={() => (logined ? (<App logined={logined} setLogout={this.setLogout} />) : (<Redirect to='/signin' />))}/>
          <Route path="/parking/:id?" component={ParkingDetail} />
          <Route path="/addcar" component={AddCar} />
          <Route path="/payment" component={Payment} />
          <Route path="/thankyou" render={()=><ThankyouForm  setLogin={this.setLogin}/>} />
          <Route path='/parking/Register' render={() => (<WrappedRegistrationForm logined={logined} setLogin={this.setLogin} />)} />
          <Route path='/parking/Resetpassword' render={() => (< WrappedResetpasswordForm logined={logined} setLogin={this.setLogin} />)} />
        </Switch >
      </Router>
    )
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired
}

// const PrivateRoute = ({ component: Component, ...rest }) => {
//   // console.log(await checkAPI())
//   return (
//     <Route {...rest} render={props => data ? (<Component {...props} />) : (<Redirect to={{ pathname: '/' }} />)} />
//   )
// }

// state: { from: props.location }
export default Root
