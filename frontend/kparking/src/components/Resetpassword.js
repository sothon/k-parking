import React from 'react'
import { Form, Input, Select, Button, AutoComplete } from 'antd';
import './Register.css'
import axios from 'axios'
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;


class Resetpassword extends React.Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async (err, values) => {
            const tokenfromdata = await axios({
                url: "http://localhost:3000/parking/resetpassword",
                method: "post"
                ,
                data: {
                    'email': values.email,
                }
            })
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }



    handleWebsiteChange = (value) => {
        let autoCompleteResult;
        if (!value) {
            autoCompleteResult = [];
        } else {
            autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
        }
        this.setState({ autoCompleteResult });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        // const { autoCompleteResult } = this.state;



        return (
            <Form onSubmit={this.handleSubmit} style={{ paddingTop: '10%', textAlign: "center" }} >
                <FormItem

                    label="E-mail"
                >
                    {getFieldDecorator('email', {
                        rules: [{
                            type: 'email', message: 'The input is not valid E-mail!',
                        }, {
                            required: true, message: 'Please input your E-mail!',
                        }],
                    })(
                        <Input style={{ width: '70%' }} />
                    )}
                </FormItem>
                <FormItem >
                    <Button type="primary" htmlType="submit">Reset Password</Button>
                </FormItem>
            </Form>
        );
    }
}

const WrappedResetpasswordForm = Form.create()(Resetpassword);
export default WrappedResetpasswordForm