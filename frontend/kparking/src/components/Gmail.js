import React, {
    Component
} from 'react'
import {
    GoogleLogin
} from 'react-google-login';

import {
    Form,
    Icon,
    Input,
    Button,
    Checkbox,
    Row,
    Col
} from 'antd';
import './facebook.css';
import {
    login
} from "./api"
import axios from "axios"
const FormItem = Form.Item;

class Login extends Component {
    constructor(props, context) {
        super(props, context);
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    responseFacebook = async (response) => {
        const tokenfromdata = await axios({
            url: "http://localhost:3000/parking/insertcustomerbyfacebook",
            method: "post",
            headers: {
                authorization: JSON.stringify(response)
            }
        })
        // console.log(a)
        // console.log(response);



        window.sessionStorage.setItem('jwtToken', tokenfromdata.data.token);

        window.location.href = `https://localhost:3000/map`
        // window.location.href = `https://app.kparking.co/map`

        //window.location.href = `https://www.google.com/maps/search/?api=1&query=${this.state.parking[0].lat},${this.state.parking[0].lng}`
        //anything else you want to do(save to localStorage)...

    }

    render() {
        const {
            getFieldDecorator
        } = this.props.form;
        const responseFacebook = (response) => {
            console.log(response);
            this.signup(response, 'facebook');
        }

        return (

            <Row >
            <Col xs = {2} sm = {4} md = {4} lg = {4} xl = {4}> </Col> 
            <Col xs = {20}sm = {16}md = {16}lg = {16}xl = {16} >
            <div style = {
                {
                    // height: '80%',
                    paddingTop: '30%',
                    // paddingBottom: '',
                    margin: '0',
                    backgroundImage: `url('/images/kparking/bg.png')`,
                    backgroundPosition: 'center',
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                }
            } >
            <Form style = {{textAlign: 'center'}} > 
            { /* <Form onSubmit={this.handleSubmit} className="login-form"> */ } 
            {/* <FormItem style={{marginTop:'50%'}}>{getFieldDecorator('userName', {rules: [{ required: true, message: 'Please input your username!' }],})( */} 
            {/* <FormItem><Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" /></FormItem> */} 
            { /* )} */ }
            { /* </FormItem> */ }
            {/* <FormItem>{getFieldDecorator('password', {rules: [{ required: true, message: 'Please input your Password!' }],})( */} 
            {/* <FormItem style={{marginTop:'-20px'}}><Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" /></FormItem> */} 
            {/* )} </FormItem> */} 
            {/* <FormItem ><Button type="primary" htmlType="submit" className="login-form-button" style={{width:'70%'}}>Log in</Button><br /></FormItem> */}

            <FormItem style = {{paddingBottom: '60%'}} >
            <div style = {{color: '#ffffff',display: 'inline'}} >
            <span style = {{fontSize: '70px'}} > K </span> 
            <span style = {{fontSize: '57px'}} > P </span> <span style = {{fontSize: '46px'}} > ARKING </span> 
            </div > <div style = {{ color: '#ffffff'}} >
            <p style = {{fontSize: '14px',lineHeight: '4px',marginTop: '-8px'}} > ANYTIME ANYPLACE ANYPRICE </p> 
            </div > 
            </FormItem>


            <FormItem >
            <FacebookLogin 
            href = "/map"
            socialId = "344853606260075"
            language = "en_US"
            scope = "public_profile,email"
            responseHandler = {this.responseFacebook}
            xfbml = {true}
            fields = "id,email,name"
            version = "v2.5"
            className = "loginBtn loginBtn--facebook"
            buttonText = "Login with Facebook"
            />
            <button class = "loginBtn loginBtn--gmail" > Login with Gmail </button> 
            </FormItem> 
            <FormItem style = {{ paddingBottom: '10%'}} >
            <div style = {{color: '#ffffff'}} >
            <p style = {{fontSize: '13px',lineHeight: '4px',marginTop: '10px'}} > Parking lot owner ? </p> </div > 
            </FormItem> </Form > </div>                      
            </Col > <Col xs = {2}
            sm = {4}
            md = {4}
            lg = {4}
            xl = {4} > 
            </Col> </Row >

        );
    }
}
const WrappedNormalLoginForm = Form.create()(Login);
export default WrappedNormalLoginForm




{
    /* <FacebookLogin socialId="110160526487317"
                           language="en_US"
                           scope="public_profile,email"
                           responseHandler={this.responseFacebook}
                           xfbml={true}
                           fields="id,email,name"
                           version="v2.5"
                           className="facebook-login"
                           buttonText="Login With Facebook"/> */
}