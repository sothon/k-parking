import React, { Component } from 'react'
import './App.css';
import { Icon, Input, AutoComplete } from 'antd';


const Option = AutoComplete.Option;

class complete extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // dataSource: db.parkings,
      dataSource: [],
    }
  }

  componentWillMount() {    
    this.setState({ 
      dataSource: []
    });
  }

  componentDidMount() {
    const url = `https://app.kparking.co/api/parking/`
    fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({ 
          dataSource: data.parkings 
        });

      });
  }    

  onSelect = (value) => {
    //let results = db.parkings.filter((val, index) => {
    let results = this.state.dataSource.filter((val, index) => {
      return (val._id === value ? val : null)
    })
    this.props.onSearchChange(results);
  }

  handleSearch = (value) => {
    if (value === '') {
      this.props.onSearchChange(this.state.dataSource);
    } 
  }
  
  renderOption = (item) => {
    return (   
      <Option key={item._id} text={item.title}>
        {item.title}      
      </Option>
    );
  }

  render() {
    const { dataSource } = this.state;
    return (
      <div className="global-search-wrapper" style={{ width: '100%' }}>
        <AutoComplete
          className="global-search"
          size="large"
          style={{ width: '100%' }}
          dataSource={dataSource.map(this.renderOption)}
          onSelect={this.onSelect}
          onSearch={this.handleSearch}
          filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
          placeholder="Where are you headed?"
          optionLabelProp="text"
        >
          <Input suffix={<Icon type="search" className="certain-category-icon" />} />
        </AutoComplete>              
      </div>
    );
  }
}

export default complete;