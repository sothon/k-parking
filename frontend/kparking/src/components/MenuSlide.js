import React, { Component } from 'react'
import { Menu, Icon, Drawer,Card } from 'antd';
import { Link } from 'react-router-dom'

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

export default class MenuSlide extends Component {
    handleClick = (e) => {
        console.log('click ', e);
        if(e.key === 'LogOut'){
            this.props.setLogout()
        }
      }
    
  render() {
    return (
        <Drawer
        // title='Menu'
        placement='right'
        closable={false}
        onClose={()=>this.props.onClose()}
        visible={this.props.visible}
        style={{padding:0}}
        >
            <Menu
            onClick={this.handleClick}
            style={{ width: 256 }}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
            >
                <MenuItemGroup key="g1" title="My Profile">
                    <Menu.Item key="History"><Icon type="hdd" />History</Menu.Item>
                    <Menu.Item key="ChangeCar"><Icon type="car" />Change Car</Menu.Item>
                    <Menu.Item key="ChangePayment"><Icon type="wallet" />Change Payment</Menu.Item>
                </MenuItemGroup>
                <MenuItemGroup key="g2" title="Customer Support">
                    <Menu.Item key="Settings"><Icon type="setting" />Settings</Menu.Item>
                    <Menu.Item key="Help"><Icon type="fork" />Help</Menu.Item>
                    <Menu.Item key="LogOut" ><Link to='/' ><Icon type="export" />Log out</Link></Menu.Item>
                </MenuItemGroup>
            </Menu>
      </Drawer>
    )
  }
}
