import React from 'react'
import { Form, Input, Tooltip, Icon, Select, Button, AutoComplete } from 'antd';
import axios from 'axios'
import './Register.css'
import {
    //  Route,
     Redirect
     } from 'react-router'
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;


class RegistrationForm extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            redirectregister: false
        }
    }
    setredirect = () => {
        this.setState({ redirectregister: true })
    }

    handleRegister = async (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(async (err, values) => {
            const tokenfromdata = await axios({
                url: "http://localhost:3000/parking/createcustomerbyemail",
                method: "post"
                ,
                data: {
                    'email': values.email,
                    'password': values.password,
                    'firstname': values.firstname,
                    'lastname': values.lastname,
                    'facbook': "",
                    'gmail': "",
                    'typelogin': "email"
                }

            })

            if (tokenfromdata.data.have) {
                this.props.setLogin()
                this.setredirect()
                window.sessionStorage.setItem('jwtToken', tokenfromdata.data.token);
            } else {
                this.props.setLogin()
                this.setredirect()
                window.sessionStorage.setItem('jwtToken', tokenfromdata.data.token);
            }
        });
    }

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    }

    handleWebsiteChange = (value) => {
        let autoCompleteResult;
        if (!value) {
            autoCompleteResult = [];
        } else {
            autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
        }
        this.setState({ autoCompleteResult });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        // const { autoCompleteResult } = this.state;
        // const { redirectregister } = this.state.redirectregister;

        return (
            <div>
                {/* // redirect after success register user */}
                {this.state.redirectregister && <Redirect to='/map' />}

                <Form onSubmit={this.handleRegister} style={{ paddingTop: '10%', textAlign: "center" }} >
                    <FormItem

                        label={(
                            <span>
                                Firstname&nbsp;
              <Tooltip title="What do you want others to call you?">
                                    <Icon type="question-circle-o" />
                                </Tooltip>
                            </span>
                        )}
                    >
                        {getFieldDecorator('firstname', {
                            rules: [{ required: true, message: 'Please input your Firstname!', whitespace: true }],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem

                        label={(
                            <span>
                                Lastname&nbsp;
              <Tooltip title="What do you want others to call you?">
                                    <Icon type="question-circle-o" />
                                </Tooltip>
                            </span>
                        )}
                    >
                        {getFieldDecorator('lastname', {
                            rules: [{ required: true, message: 'Please input your lastname!', whitespace: true }],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem

                        label="E-mail"
                    >
                        {getFieldDecorator('email', {
                            rules: [{
                                type: 'email', message: 'The input is not valid E-mail!',
                            }, {
                                required: true, message: 'Please input your E-mail!',
                            }],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem

                        label="Password"
                    >
                        {getFieldDecorator('password', {
                            rules: [{
                                required: true, message: 'Please input your password!',
                            }, {
                                validator: this.validateToNextPassword,
                            }],
                        })(
                            <Input type="password" />
                        )}
                    </FormItem>
                    <FormItem

                        label="Confirm Password"
                    >
                        {getFieldDecorator('confirm', {
                            rules: [{
                                required: true, message: 'Please confirm your password!',
                            }, {
                                validator: this.compareToFirstPassword,
                            }],
                        })(
                            <Input type="password" onBlur={this.handleConfirmBlur} />
                        )}
                    </FormItem>


                    <FormItem >
                        <Button type="primary" htmlType="submit">Register</Button>

                    </FormItem>
                </Form>
            </div>
        );
    }
}

const WrappedRegistrationForm = Form.create()(RegistrationForm);
export default WrappedRegistrationForm