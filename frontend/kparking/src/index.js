import React from 'react'
import { render } from 'react-dom'
// import { createStore } from 'redux'
// import kparkingApp from './reducers'
import Root from './components/Root'

let store = {}
render(<Root store={store} />, document.getElementById('root'))
