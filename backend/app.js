'use strict'

const fs = require('fs')
const Koa = require('koa')
const cors = require('koa2-cors')
const Router = require('koa-router')
const session = require('koa-session')
const bodyParser = require('koa-bodyparser')
const bcrypt = require('bcrypt')
const mongo = require('koa-mongo')
const AirPayconfig = require('./config/configairpay')
const kparkingConfig = require('./config/.env.local')
const jwtConfig = require('./config/configjwt.json')
const jwt = require('jsonwebtoken')
const nodemailer = require('nodemailer')

const app = new Koa()
const router = new Router()
// Verify Token
async function Authorization (ctx, next) {
  // Get auth header value
  console.log('Authorization')
  const bearerHeader = ctx.headers['authorization']

  // Check if bearer is undefined
  if (typeof bearerHeader !== 'undefined') {
    // Split at the space
    const bearer = bearerHeader.split(' ')
    // Get token from array
    const bearerToken = bearer[1]
    // Set the token
    console.log(bearerToken)
    ctx.token = bearerToken
    // Next middleware
    next()
  } else {
    // Forbidden"
    ctx.status = 403
  }
}
app.use(cors({
  origin: function (ctx) {
    if (ctx.url === '/test') {
      return false
    }
    return '*'
  },
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
  maxAge: 5,
  credentials: true,
  allowMethods: ['OPTIONS', 'GET', 'POST', 'PUT', 'DELETE'],
  allowHeaders: ['Content-Type', 'Authorization', 'Accept']
}))

app.use(mongo({
  host: kparkingConfig.dbHost,
  port: kparkingConfig.dbPort,
  user: kparkingConfig.dbUser,
  pass: kparkingConfig.dbPassword,
  db: kparkingConfig.dbName
}))

app.use(async (ctx, next) => {
  if (ctx.path !== '/favicon.ico') {
    return next()
  }
})

app.use(bodyParser())

// const router = require('./routes/router')
// const sessionConfig = require("./libs/session");
// const author = require("./libs/authorize");

// app.keys = ["supersecret"];

// app.use(session(sessionConfig.config, app))
// app.use(author.authorize)

app.use(async (ctx, next) => {
  const start = process.hrtime()
  await next()
  const diff = process.hrtime(start)
  console.log(
    `${ctx.method} - ${ctx.status} - ${ctx.path} - ${ctx.type} - ( ${diff[0] *
    1e9 +
    diff[1]}ns )`
  )
})

app.use(async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    console.log('Server Error: ' + err)
    ctx.status = 500
    ctx.body = 'Error : 500'
  }
})

router.get('/', async (ctx) => {
  ctx.body = 'Kparking'
})
  .get('/parking', async (ctx) => {
    let result = await ctx.mongo.db('kparking').collection('parking').find().sort({
      title: 1
    }).toArray()
    ctx.body = {
      'parkings': result
    }
  })
  .get('/parking/:id', async (ctx) => {
    let result = await ctx.mongo.db('kparking').collection('parking').find({
      _id: mongo.ObjectId(ctx.params.id)
    }).toArray()
    ctx.body = {
      'parkings': result
    }
  })

  .put('/parking/:id/checkin', Authorization, async (ctx) => {
    await ctx.mongo.db('kparking').collection('parking').update({
      _id: mongo.ObjectId(ctx.params.id)
    }, {
      $inc: {
        'number_park': 1
      }
    })

    let result = await ctx.mongo.db('kparking').collection('parking').find({
      _id: mongo.ObjectId(ctx.params.id)
    }).toArray()

    let data_number_max = Number(result[0].number_max) || 0
    let data_number_park = Number(result[0].number_park) || 0

    if (data_number_park > data_number_max) {
      if (data_number_park > 0) {
        await ctx.mongo.db('kparking').collection('parking').update({
          _id: mongo.ObjectId(ctx.params.id)
        }, {
          $inc: {
            'number_park': -1
          }
        })
      }
      ctx.body = {
        'checkin': false
      }
    } else {
      ctx.body = {
        'checkin': true
      }
    };
  })

  .put('/parking/:id/checkout', async (ctx) => {
    await ctx.mongo.db('kparking').collection('parking').update({
      _id: mongo.ObjectId(ctx.params.id)
    }, {
      $inc: {
        'number_park': -1
      }
    })

    let result = await ctx.mongo.db('kparking').collection('parking').find({
      _id: mongo.ObjectId(ctx.params.id)
    }).toArray()

    let data_number_park = Number(result[0].number_park) || 0

    if (data_number_park < 0) {
      await ctx.mongo.db('kparking').collection('parking').update({
        _id: mongo.ObjectId(ctx.params.id)
      }, {
        $inc: {
          'number_park': 0
        }
      })
    };
    ctx.body = {
      'checkout': true
    }
  })

  .put('/parking/:id/reset', async (ctx) => {
    await ctx.mongo.db('kparking').collection('parking').update({
      _id: mongo.ObjectId(ctx.params.id)
    }, {
      $set: {
        'number_park': 0
      }
    })

    let result = await ctx.mongo.db('kparking').collection('parking').find({
      _id: mongo.ObjectId(ctx.params.id)
    }).toArray()
    ctx.body = {
      'parkings': result
    }
  })

  .get('/api/admin/create', async (ctx) => {
    await ctx.mongo.db('kparking').createCollection('parking')
    ctx.body = await ctx.mongo.db('kparking').collection('parking').find().toArray()
  })
  .get('/api/admin/insert', async (ctx) => {
    await ctx.mongo.db('kparking').collection('parking').insert(
      [{
        'title': 'ที่จอดรถ สวนสมเด็จพระนางเจ้าสิริกิติ์ ฝั่งถนนกำแพงเพชร 4',
        'lat': 13.803261,
        'lng': 100.5499485,
        'address': '',
        'phone': '',
        'photo': '',
        'thumbnail': '',
        'marker1x': '/images/icons/premiumIcon.png',
        'marker2x': '/images/icons/premiumIcon@2x. png',
        'marker3x': '/images/icons/premiumIcon@3x.png',
        'markerType': 1,
        'number_max': 0,
        'number_park': 0,
        'open': '',
        'price': [],
        'detail': [],
        'landmark': '',
        'facilitate': [],
        'owner': '',
        'rate': 0,
        'created_at': new Date()
      },
      {
        'title': 'JJ Park',
        'lat': 13.803302,
        'lng': 100.5517744,
        'address': '',
        'phone': '',
        'photo': '',
        'thumbnail': '',
        'marker1x': '/images/icons/freeIcon.png',
        'marker2x': '/images/icons/freeIcon@2x. png',
        'marker3x': '/images/icons/freeIcon@3x.png',
        'markerType': 0,
        'number_max': 0,
        'number_park': 0,
        'open': '',
        'price': [],
        'detail': [],
        'landmark': '',
        'facilitate': [],
        'owner': '',
        'rate': 1,
        'created_at': new Date()
      },
      {
        'title': 'ศูนย์การค้า SOHO',
        'lat': 13.7502301,
        'lng': 100.5141336,
        'address': 'ศูนย์การค้า Soho, B1 Bamrung Muang Rd, Khwaeng Khlong Maha Nak, Khet Pom Prap Sattru Phai, Krung Thep Maha Nakhon 10100',
        'phone': '02-223-0871',
        'photo': '',
        'thumbnail': '',
        'marker1x': '/images/icons/premiumIcon.png',
        'marker2x': '/images/icons/premiumIcon@2x. png',
        'marker3x': '/images/icons/premiumIcon@3x.png',
        'markerType': 1,
        'number_max': 0,
        'number_park': 0,
        'open': '10:00-22:00 น.',
        'price': [],
        'detail': [],
        'landmark': '',
        'facilitate': [],
        'owner': '',
        'rate': 5,
        'created_at': new Date()
      },
      {
        'title': 'ตลาดนัดมะลิ',
        'lat': 13.9260646,
        'lng': 100.5438387,
        'address': '111 แจ้งวัฒนะ-ปากเกร็ด 39, บ้านใหม่, ปากเกร็ด, นนทบุรี 11120',
        'phone': '',
        'photo': '/images/kparking/mali_market.jpg',
        'thumbnail': '/images/kparking/mali_market_thumbnail.jpg',
        'marker1x': '/images/icons/premiumIcon.png',
        'marker2x': '/images/icons/premiumIcon@2x. png',
        'marker3x': '/images/icons/premiumIcon@3x.png',
        'markerType': 1,
        'number_max': 0,
        'number_park': 0,
        'open': '06:00-23:59 น.',
        'price': [],
        'detail': [],
        'landmark': '',
        'facilitate': [],
        'owner': '',
        'rate': 4,
        'created_at': new Date()
      },
      {
        'title': 'ที่จอดรถ เขตอุตสาหกรรมซอฟต์แวร์ประเทศไทย',
        'lat': 13.9042099,
        'lng': 100.529741,
        'address': '99/31 หมู่4 ซอย แจ้งวัฒนะ-ปากเกร็ด 19 ตำบลคลองเกลือ อำเภอ ปากเกร็ด นนทบุรี 11120',
        'phone': '',
        'photo': '',
        'thumbnail': '',
        'marker1x': '/images/icons/freeIcon.png',
        'marker2x': '/images/icons/freeIcon@2x. png',
        'marker3x': '/images/icons/freeIcon@3x.png',
        'markerType': 0,
        'number_max': 100,
        'number_park': 0,
        'open': '',
        'price': [],
        'detail': [
          'จอดรถได้ฟรี',
          'จอดรถในตึก'
        ],
        'landmark': 'ตรงข้าม Central แจ้งวัฒนะ',
        'facilitate': [],
        'owner': '',
        'rate': 5,
        'created_at': new Date()
      },
      {
        'title': 'ที่จอดรถ ตลาดกันเอง แจ้งวัฒนะ-ปากเกร็ด 19',
        'lat': 13.9050303,
        'lng': 100.529328,
        'address': 'ซอยแจ้งวัฒนะ-ปากเกร็ด19 ถนนแจ้งวัฒนะ ตำบล ปากเกร็ด อำเภอ ปากเกร็ด นนทบุรี 11120',
        'phone': '',
        'photo': '',
        'thumbnail': '',
        'marker1x': '/images/icons/freeIcon.png',
        'marker2x': '/images/icons/freeIcon@2x.png',
        'marker3x': '/images/icons/freeIcon@3x.png',
        'markerType': 0,
        'number_max': 50,
        'number_park': 0,
        'open': '06:00-23:59 น.',
        'price': [
          'รายวัน : 50 บาท'
        ],
        'detail': [
          'เปิดบริการ 06:00-23:59 น.',
          'จอดรถ 50 บาท/วัน',
          'มีที่จอดรถ 50 คัน'
        ],
        'landmark': '',
        'facilitate': [],
        'owner': '',
        'rate': 3,
        'created_at': new Date()
      },
      {
        'title': 'ที่จอดรถ V-GROUP แจ้งวัฒนะ-ปากเกร็ด 21',
        'lat': 13.9056643,
        'lng': 100.5308696,
        'address': 'ซอยแจ้งวัฒนะ-ปากเกร็ด21 ถนนแจ้งวัฒนะ ตำบลคลองเกลือ อำเภอ ปากเกร็ด นนทบุรี 11120',
        'phone': '',
        'photo': '',
        'thumbnail': '',
        'marker1x': '/images/icons/freeIcon.png',
        'marker2x': '/images/icons/freeIcon@2x.png',
        'marker3x': '/images/icons/freeIcon@3x.png',
        'markerType': 0,
        'number_max': 30,
        'number_park': 0,
        'open': '06:00-23:59 น.',
        'price': [
          'รายวัน : 30 บาท'
        ],
        'detail': [
          'เปิดบริการ 06:00-23:59 น.',
          'จอดรถ 30 บาท/วัน',
          'มีที่จอดรถ 30 คัน'
        ],
        'landmark': '',
        'facilitate': [],
        'owner': '',
        'rate': 2,
        'created_at': new Date()
      },
      {
        'title': 'ที่จอดรถ BTS บางหว้า ปั๊มเซลล์(Shell) กรุงเทพมหานคร',
        'lat': 13.7205208,
        'lng': 100.4558348,
        'address': '204/25 ถนนเพชรเกษม34 เเขวงปากคลองภาษีเจริญ เขตภาษีเจริญ กรุงเทพมหานคร 10160',
        'phone': '',
        'photo': '',
        'thumbnail': '',
        'marker1x': '/images/icons/iconSkytrain.png',
        'marker2x': '/images/icons/iconSkytrain@2x.png',
        'marker3x': '/images/icons/iconSkytrain@3x.png',
        'markerType': 2,
        'number_max': 15,
        'number_park': 0,
        'open': '05:00-23:00 น.',
        'price': [
          'รายวัน : 50 บาท'
        ],
        'detail': [
          'สถานีบริการน้ำมันตั้งอยู่บนถนนเพชรเกษม ห่างจากประตูมหาวิทยาลัยสยาม 400 เมตรเเละห่างจากจากสถานี BTSบางหว้าประมาณ 200 เมตร มีที่จอดรถ15 คัน หากเติมน้ำมันที่ปั๊มสามารถจอดได้ฟรีตลอดทั้งวัน',
          'เปิดบริการ 05:00 - 23:00น.',
          'จอดรถได้ฟรี 20 นาที เกิน 20 นาที 50 บาท/วัน',
          'มีที่จอดรถ 15 คัน',
          'หากเติมน้ำมันที่ปั๊มสามารถจอดได้ฟรีตลอดทั้งวัน'
        ],
        'landmark': '',
        'facilitate': [],
        'owner': '',
        'rate': 3,
        'created_at': new Date()
      }
      ]
    )
    ctx.body = await ctx.mongo.db('kparking').collection('parking').find().sort({
      title: 1
    }).toArray()
  })
  .post('/parking/login', async (ctx) => {
    let cusdetail = ctx.request.body
    console.log(cusdetail)
    let findemail = await ctx.mongo.db('kparking').collection('customers').find({
      email: cusdetail.email
    }).toArray()
    console.log(findemail.length)
    if (findemail.length > 0) {
      let dbpassword = findemail[0]['password']
      let math = bcrypt.compareSync(cusdetail.password, dbpassword)
      console.log(math)
      if (math) {
        const token = jwt.sign({
          id: findemail._id
        }, jwtConfig.secret, {
          // expiresIn: 86400 // expires in 24 hours
          expiresIn: 180 // expires in 3 min
        })
        ctx.body = {
          'token': token
        }
      } else {
        ctx.body = false
      }
    } else {
      ctx.body = { 'token': '0' }
    }
  })
  .post('/parking/resetpassword', async (ctx) => {
    let cusdetail = ctx.request.body

    let findemail = await ctx.mongo.db('kparking').collection('customers').find({
      email: cusdetail.email
    }).toArray()

    if (findemail.length === 0) {
      ctx.body = []
    } else {
      let randompassword = Math.random().toString(36).substring(2, 6) + Math.random().toString(36).substring(2, 6)
      let hashpassword = bcrypt.hashSync(randompassword, 10).toString()
      // config สำหรับของ gmail
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'max0899561914@gmail.com', // your email
          pass: '27145489' // your email password
        }
      })
      let mailOptions = {
        from: 'max0899561914@gmail.com',
        to: cusdetail.email,
        subject: 'Hello from sender',
        html: '<b>Do you receive this mail?</b>'
      }
      transporter.sendMail(mailOptions, function (err, info) {
        if (err) { console.log(err) } else { console.log(info) }
      })
      await ctx.mongo.db('kparking').collection('customers').update({
        _id: mongo.ObjectId(findemail._id)
      }, {
        $set: {
          'password': hashpassword
        }
      })
      const token = jwt.sign({
        id: findemail._id
      }, jwtConfig.secret, {
        // expiresIn: 86400 // expires in 24 hours
        expiresIn: 180 // expires in 3 min
      })
      ctx.body = {
        'token': token
      }
    }
  })
  .post('/parking/createcustomerbyemail', async (ctx) => {
    let cusdetail = ctx.request.body
    console.log(cusdetail.password)
    let findemail = await ctx.mongo.db('kparking').collection('customers').find({
      email: cusdetail.email
    }).toArray()
    let hashpassword = bcrypt.hashSync(cusdetail.password, 10).toString()
    if (findemail.length === 0) {
      console.log('have')
      await ctx.mongo.db('kparking').collection('customers').insertOne({
        'firstname': cusdetail.firstname,
        'lastname': cusdetail.lastname,
        'email': cusdetail.email,
        'password': hashpassword,
        'facbook': cusdetail.facbook,
        'gmail': cusdetail.gmail,
        'typelogin': cusdetail.typelogin,
        'create_date': new Date()
      })
      const token = jwt.sign({
        id: findemail._id
      }, jwtConfig.secret, {
        // expiresIn: 86400 // expires in 24 hours
        expiresIn: 180 // expires in 3 min
      })
      ctx.body = {
        'have': true,
        'token': token
      }
    } else {
      ctx.body = {
        'have': false
      }
    }
  })
  .post('/parking/insertcustomerbyfacebook', async (ctx) => {
    console.log(ctx.request.headers)
    let facbookdata = JSON.parse(ctx.request.headers.authorization)
    console.log(facbookdata.accessToken)
    let findfacbook = await ctx.mongo.db('kparking').collection('customers').find({
      facbook: facbookdata._id
    }).toArray()
    console.log(findfacbook.length)
    if (findfacbook.length === 0) {
      await ctx.mongo.db('kparking').collection('customers').insertOne({
        'firstname': facbookdata.name.toString(),
        'lastname': '',
        'email': facbookdata.email.toString(),
        'password': '',
        'facebook': facbookdata.id.toString(),
        'gmail': '',
        'typelogin': 'facebook',
        'create_date': new Date()
      })
      const token = jwt.sign({
        id: facbookdata._id,
        accessToken: facbookdata.accessToken
      }, jwtConfig.secret, {
        // expiresIn: 86400 // expires in 24 hours
        expiresIn: 180 // expires in 3 min
      })
      ctx.body = {
        'token': token
      }
    } else {
      const token = jwt.sign({
        id: facbookdata.accessToken
      }, jwtConfig.secret, {
        // expiresIn: 86400 // expires in 24 hours
        expiresIn: 180 // expires in 3 min
      })
      ctx.body = {
        'token': token
      }
    }
  })
  .post('/parking/insertcustomerbygmail', async (ctx) => {
    console.log(ctx.request.headers)
    let facbookdata = ctx.request.headers.authorization
    console.log(facbookdata.access_Token)
    let cusdetail = ctx.request.body
    let findgmail = await ctx.mongo.db('kparking').collection('customers').find({
      gmail: cusdetail.gmail
    }).toArray()
    console.log(findgmail.length)
    if (findgmail.length === 0) {
      await ctx.mongo.db('kparking').collection('customers').insertOne({
        'firstname': cusdetail.firstname,
        'lastname': cusdetail.lastname,
        'email': cusdetail.email,
        'password': cusdetail.password,
        'facbook': cusdetail.facbook,
        'gmail': cusdetail.gmail,
        'typelogin': cusdetail.typelogin,
        'create_date': new Date()
      })
      const token = jwt.sign({
        id: findgmail._id,
        accessToken: facbookdata.accessToken
      }, jwtConfig.secret, {
        // expiresIn: 86400 // expires in 24 hours
        expiresIn: 180 // expires in 3 min
      })
      ctx.body = {
        'token': token
      }
    } else {
      const token = jwt.sign({
        id: findgmail._id,
        accessToken: facbookdata.accessToken
      }, jwtConfig.secret, {
        // expiresIn: 86400 // expires in 24 hours
        expiresIn: 180 // expires in 3 min
      })
      ctx.body = {
        'token': token
      }
    }
  })

  .post('/api/notify_order_status ', Authorization, async (ctx) => {
    let notifyOrderStatus = ctx.request.body
    ctx.body = {
      'order_id': notifyOrderStatus.order_id
    }
  })

  .post('/api/validate_order', async (ctx) => {
    let result = await ctx.mongo.db('kparking').collection('customers').find({
      _id: mongo.ObjectId('5bd9cb1bb0aaf85278b1ef75')
    }).toArray()
    ctx.body = {
      'result': result
    }
  })

  .post('/api/order_detail', async (ctx) => {
    let validateOrder = ctx.request.body
    // app_key app_key The Merchant Key given by AirPay.
    //  order_id order_id Original order ID generated by Merchant.
    let findorder = await ctx.mongo.db('kparking').collection('order').find({
      _id: mongo.ObjectId('5bd9cb1bb0aaf85278b1ef75')
    }).toArray()
    let findparking = await ctx.mongo.db('kparking').collection('parking').find({
      _id: mongo.ObjectId('5aa25bc5cc9532377c49f009')
    }).toArray()
    ctx.body = {
      'order_validity': 200,
      'order': {
        'app_id': AirPayconfig.app_id,
        'order_id': validateOrder.order_id,
        'currency': 'THB',
        'payable_ amount': 1000,
        'expiry_time': 1448879325,
        'item_name': findparking.title,
        'item_image': '',
        'extra_data': ''
      }
    }
  })

  .post('/api/create_order_detail', async (ctx) => {
    let getOrderDetails = ctx.request.body
    console.log(getOrderDetails.user_id)
    console.log(getOrderDetails.item_id)
    const result = await ctx.mongo.db('kparking').collection('order').insertOne({
      // 'user_id': mongo.ObjectId(getOrderDetails.user_id),
      // 'item_id': mongo.ObjectId(getOrderDetails.item_id),
      'create_date': new Date(),
      'expiry_time': '',
      'order_status': ''
    })
    ctx.body = { result }
  })

  .post('/api/posts', Authorization, async (ctx) => {
    jwt.verify(ctx.token, jwtConfig.secret, (err, authData) => {
      console.log('aut' + authData)
      if (authData != undefined) {
        console.log('true')
        ctx.body = true
      } else {
        console.log('false')
        ctx.body = false
      }
    })
  })

app.use(router.routes())
app.use(router.allowedMethods())

app.listen(kparkingConfig.listenPort, () => {
  console.log('listening on port ' + kparkingConfig.listenPort)
})
