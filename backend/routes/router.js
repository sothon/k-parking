'use strict'

const Router = require('koa-router')
const router = new Router()

const auth = require('../controllers/authController')

router
  .get('/', async ctx => {
    ctx.body = 'homepage'
  })

  // Auth
  .post('/auth/signup', auth.signup)
  .post('/auth/signin', auth.signin)
  .get('/auth/signout', auth.signout)
  .post('/auth/verify', auth.verify)

  // Parking
  .get('/parking', async ctx => {
    ctx.body = `${ctx.method} ${ctx.path}`
  })
  .post('/parking', async ctx => {
    ctx.body = `${ctx.method} ${ctx.path}`
  })

  .put('/parking/:id', async ctx => {
    ctx.body = `${ctx.method} ${ctx.path}`
  })
  .delete('/parking/:id', async ctx => {
    ctx.body = `${ctx.method} ${ctx.path}`
  })

module.exports = router
