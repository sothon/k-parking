"use strict"

const mongo = require('koa-mongo');
const dbConfig = require('../config/dbConfigMongo.js');

exports.getConnection = async () => {
  mongo({
    host: dbConfig.host,
    port: dbConfig.port,
    user: dbConfig.user,
    pass: dbConfig.pass,
    db: dbConfig.db
  });
  return await next();  
};