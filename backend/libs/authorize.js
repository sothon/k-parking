'use strict'

const _ = require('lodash')

exports.authorize = async function (ctx, next) {
  // ignore favicon
  if (ctx.path === '/favicon.ico') return
  if (ctx.method === 'POST' && (ctx.path === '/auth/signin') || (ctx.path === '/auth/signup')) {
    let n = ctx.session.views || 0
    ctx.session.views = ++n
    return await next()
  } else {
    if (!_.isEmpty(ctx.session.user)) {
      // console.log('000')
      let n = ctx.session.views || 0
      ctx.session.views = ++n
      return await next()
    } else {
      // console.log('333')
      ctx.body = { 'error': 'unauthorized' }
      return
    }
  }
}
