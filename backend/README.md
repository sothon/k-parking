## Developer

- Mr. Sothon Vongsripeng (toey)
- Miss. Tunyarat Mokon (ink)
- Mr. Natthapone Chankongsuwan (nhan)

# Reference

- https://developers.google.com/maps/documentation/javascript/examples/marker-animations-iteration
- https://tomchentw.github.io/react-google-maps/#documentation
- https://medium.com/dumpsayamrat/%E0%B8%AA%E0%B8%A3%E0%B9%89%E0%B8%B2%E0%B8%87%E0%B9%81%E0%B8%AD%E0%B8%9B-twitter-%E0%B9%81%E0%B8%9A%E0%B8%9A%E0%B8%87%E0%B9%88%E0%B8%B2%E0%B8%A2%E0%B9%86-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-react-%E0%B9%81%E0%B8%A5%E0%B8%B0-recompose-8542d190a86b
- https://medium.com/takemetour-engineering/introduction-to-recompose-5a3cad6095b9
- https://gist.githubusercontent.com/farrrr/dfda7dd7fccfec5474d3/raw/758852bbc1979f6c4522ab4e92d1c92cba8fb0dc/data.json

- https://www.fullstackreact.com/articles/how-to-write-a-google-maps-react-component/

- https://specify.io/how-tos/mongodb-update-documents
- https://specify.io/how-tos/find-documents-in-mongodb-using-the-mongo-shell
- https://docs.mongodb.com/manual/reference/method/db.collection.find/

- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/OPTIONS

# KParking Routes

- GET /parking
- POST /parking
- PUT /parking/:id
- DELETE /parking/:id




# Twitter Routes

## Auth

- POST /auth/signup
- POST /auth/signin
- GET /auth/signout
- POST /auth/verify

## Upload

- POST /upload

## User

- PATCH /user/:id
- PUT /user/:id/follow
- DELETE /user/:id/follow
- GET /user/:id/follow
- GET /user/:id/followed

## Tweet

- GET /tweet
- POST /tweet
- PUT /tweet/:id/like
- DELETE /tweet/:id/like
- POST /tweet/:id/retweet
- PUT /tweet/:id/vote/:voteId
- POST /tweet/:id/reply

## Notification

- GET /notification

## Direct Message

- GET /message
- GET /message/:userId
- POST /message/:userId